## Hardware setup:

Raspberry Pi model B rev2, standard Raspbian OS. SPI connection on pins 19, 23, 24 going through 1/2 voltage divider (2.2kohm + 2.2kohm) to AD9364 SPI lines. Pin 21 (SPI MISO) connected directly from AD9364 to Raspberry.

4 traces oscilloscope connected to all SPI lines to verify they are working.

SPI frequency set to 100 kHz

## Modifications to standard "generic" no-os drivers:

**In file "config.h"**


removed comment from #define HAVE_DEBUG_MESSAGES

removed comment from #define AXI_ADC_NOT_PRESENT

changed these two defines:

-> #define AD9361_DEVICE 0

-> #define AD9364_DEVICE 1

**In file "platform.h"**

removed all axiadc declarations  

**In file "platform.c"**

removed all axiadc functions

copied functions from platform_linux ones (with slight modifications)

**In file "parameters.h"**

added/modified defines

-> #define GPIO_RESET_PIN 17

-> #define SPIDEV_DEV "/dev/spidev0.0"

**In file "ad9361_conv.c"**

removed the AXI_ADC #ifndef block (useless here, for some reason caused linker errors)

**In file "ad9361_api.c"**

swapped reset GPIO levels, we have an open collector transistor driving the "resetb" pin so a high GPIO level means reset

**In file "main.c"**

gpio_init(GPIO_DEVICE_ID); changed to gpio_init(GPIO_RESET_PIN);
